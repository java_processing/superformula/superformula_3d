import noiseAlgorithms.openSimplexNoise.OpenSimplexNoise;    // OpenSimplexNoise is used to create ripples and color pattern
import peasy.PeasyCam;           // peasycam is a mouse driven camera
import controlP5.ControlP5;
import controlP5.CallbackEvent;  // events are used to prevent the shape from moving when using the sliders
import controlP5.Slider;     // Sliders are used to tweak the parameters of the supershape

ControlP5 cp5;
PeasyCam cam;
PMatrix mat;   // store the initial orientation matrix (this will be usefull to make the movement of the light independant of the movements of the peasycam)

float r_unite = 100;
float radius;   // tweakable radius
float distLight;  // distance of the light from the center of the shape
Slider sldR;   // radius tweaking slider
final String RDS = "RADIUS";  // name of the slider that controls the radius
final int resolution = 250;  // points per revolution
float timehue = 0;
final float huespd = 0.01;   // this value is added to timehue each frame to create a color animation (option 1)
final int COLOR_ANIM_1 = 1;


PVector[][] superShape = new PVector[resolution][resolution/2];  // contains the supershape

OpenSimplexNoise osNoise;

// noise to create the riples in the shape (rds stands for radius)
float noise_offset_rds = 0;
final float noise_spd_rds = 0.025;  // this value is added to noise_offset_rds each frame to create the riple animation (this is the "speed" of the noise)
final float sharp_rds = 12.5/100.;  // this value controls how sharp or smooth the noise is

// same for the color (color animation : option 2)
float noise_offset_clr = 0;
final float noise_spd_clr = 0.0125;
final float sharp_clr = 3/100.;
final int COLOR_ANIM_2 = 2;

// spherical coordinates are used to create the shape
// the 3D shape is created thanks to 2 2D supershapes that are traveled by two angles
// theta (from 0 to TWO_PI)
float[] thetaValues = new float[7];
final String[] thetaNames = {"NT1", "NT2", "NT3", "AT", "BT", "MT1", "MT2"}; //parameters of the 2D supershape (http://paulbourke.net/geometry/supershape/)
Slider[] sldTArr = new Slider[7];

// phi (from -PI to PI)
float[] phiValues = new float[7];
final String[] phiNames = {"NP1", "NP2", "NP3", "AP", "BP", "MP1", "MP2"};
Slider[] sldPArr = new Slider[7];

// minimum value, starting value, and end value for the sliders that control the parameters
final float [][] ranges_values = {
  {-1., 1., 0.2}, 
  {0, 4., 1.7}, 
  {0, 4., 1.7}, 
  {-1., 2., 1.}, 
  {-1., 2., 1.}, 
  {0., 12., 8.}, 
  {0., 12., 8.}};

void setup() {
  println("Use z, q, s, d, e and f to coltrol the light");
  println("Light movements are computed with quaternions \n");
  size(950, 950, P3D);
  frameRate(60);
  
  mat = g.getMatrix();

  cp5 = new ControlP5(this); 
  
  sldR = cp5.addSlider(RDS).setPosition(0, height-18).setRange(10, 200); // slider to control the radius of the shape (contrary to the peasycam zoom, this changes the ripples visualisation)
  sldR.setDecimalPrecision(0);
  sldR.setValue(100);
  sldR.setSize(400, 16);
  
  setThetaSliders(); // initialize the sliders for the 2D shape relative to theta
  setPhiSliders();

  cam = new PeasyCam(this, distViewer);

  osNoise = new OpenSimplexNoise();
}

public void setThetaSliders() {

  for (int i = 0; i < thetaNames.length; i++) {
    sldTArr[i] = cp5.addSlider(thetaNames[i])
      .setPosition(0, 18*i)
      .setRange(ranges_values[i][0], ranges_values[i][1]);
    if (i == 5 || i == 6) {
      sldTArr[i].setDecimalPrecision(1);
    } else {
      sldTArr[i].setDecimalPrecision(3);
      sldTArr[i].setScrollSensitivity(0.001);
    }
    sldTArr[i].setValue(ranges_values[i][2]);
    sldTArr[i].setSize(400, 16);
  }
}

public void setPhiSliders() {

  for (int i = 0; i < 7; i++) {  
    sldPArr[i] = cp5.addSlider(phiNames[i])
      .setPosition(410, 18*i)
      .setRange(ranges_values[i][0], ranges_values[i][1]);
    if (i == 5 || i == 6) {
      sldPArr[i].setDecimalPrecision(1);
    } else {
      sldPArr[i].setDecimalPrecision(3);
      sldPArr[i].setScrollSensitivity(0.001);
    }
    sldPArr[i].setValue(ranges_values[i][2]);
    sldPArr[i].setSize(400, 16);
  }
}

void draw() {
  
  // check the values of the sliders
  for (int i = 0; i < sldTArr.length; i++) {  
    thetaValues[i] = sldTArr[i].getValue();
    phiValues[i] = sldPArr[i].getValue();
  }  
  radius = sldR.getValue();

  background(0);
  noStroke();

  // compute the light 
  pushMatrix();
  g.setMatrix(mat); // this resets the initial matrix so that the light isn't affected by the peasyCam movements
  computeShowLight(); 
  popMatrix();

  // compute and show the shape
  computeShape();  // compute the shape (without noise) with the parameters of the sliders
  showShape(COLOR_ANIM_2);  // show the shape (with noise)

  cam.beginHUD();
  cp5.draw();    // display the HUD
  cp5.setAutoDraw(false);
  cam.endHUD();
}

// compute the shape

public void computeShape() {
  float theta;
  float phi;
  float x, y, z, 
    rt, rp, // radii of the shapes relative to theta and phi
    ct, cp, st, sp, // cos and sin of the angles
    nxoff, nyoff, nzoff, 
    roff;
  PVector vect;
  float sharpNoise = sharp_rds*radius;
  for (int p = 0; p < resolution/2; p++) {
    phi = map(p, 0, resolution/2, -PI/2, PI/2); 
    rp = r(phi, 
      phiValues[0], 
      phiValues[1], 
      phiValues[2], 
      phiValues[3], 
      phiValues[4], 
      phiValues[5], 
      phiValues[6]);
    cp = cos(phi);
    sp = sin(phi);
    nzoff = map(p, 0, resolution/2, 0, sharpNoise);  // noise offset for the z coordinate
    for (int t = 0; t < resolution; t++) {
      theta = map(t, 0, resolution, -PI, PI);
      rt = r(theta, 
        thetaValues[0], 
        thetaValues[1], 
        thetaValues[2], 
        thetaValues[3], 
        thetaValues[4], 
        thetaValues[5], 
        thetaValues[6]);
      ct = cos(theta);
      st = sin(theta);
      nxoff = map(ct, -1, 1, 0, sharpNoise);  // noise offset for the x coordinate
      nyoff = map(st, -1, 1, 0, sharpNoise);  // noise offset for the y coordinate  
      
      // using the 4th dimension as the time is the right way to do this but uses too much computing ressources
      //roff = (float) osNoise.eval((double) nxoff, (double) nyoff, (double) nzoff, (double) noise_offset_rds*10.);  //multiplying the time component by 10 compensate the framerate drop withtout touching the speed variable
      
      // this method allows higher framerates but leads to directionnal artefacts (the ripples have directions, which is not the case with the other method)
      roff = (float) osNoise.eval((double)(noise_offset_rds + nxoff), (double)(noise_offset_rds + nyoff), (double)(noise_offset_rds + nzoff));
      
      
      roff = map(roff, -1, 1, 0, sharpNoise);  // radius offset
      
      // conversion from spherical to cartesian
      x = radius*rt*rp*ct*cp;
      y = radius*rt*rp*st*cp;
      z = radius*rp*sp;
      vect = new PVector(x, y, z);
      superShape[t][p] = vect;
      vect.add(vect.copy().normalize().mult(roff));  // add the noise
    }
  }
  noise_offset_rds += noise_spd_rds;  // animate the noise
}

// show the shape
public void showShape(int hueMode) {
  PVector pnt, pntp;   // point, next point along phi
  for (int p = 0; p < resolution/2; p++) {
    push();
    colorMode(HSB, 100, 100, 100);
    beginShape(TRIANGLE_STRIP);
    for (int t = 0; t < resolution+1; t++) {
      fill(computeHue(hueMode, p, t), 80, 100); //compute the hue
      pnt = superShape[t%resolution][p].copy();
      vertex(pnt.x, pnt.y, pnt.z);
      pntp = superShape[t%resolution][(p+1)%(resolution/2)];
      vertex(pntp.x, pntp.y, pntp.z);

    }
    endShape();
    pop();
    if (hueMode==COLOR_ANIM_1) {
      timehue += huespd;
    }
  }
  if (hueMode==COLOR_ANIM_2) {
    noise_offset_clr += noise_spd_clr;
  }

}


public float computeHue(int hueMode, int p, int t) {
  float finalHue = 0;
  
  if (hueMode==COLOR_ANIM_1) {
    float turnhue = map(p, 0, resolution/2, 0, 100);
    float hue = map(t, 0, resolution+1, 0, 100);
    finalHue = (hue+turnhue+timehue)%100; 
  } 
  else if (hueMode==COLOR_ANIM_2) {
    float sharpNoise = sharp_clr*radius;
    float theta = map(t, 0, resolution, -PI, PI);
    float nzoff = map(p, 0, resolution/2, 0, sharpNoise);
    float ct = cos(theta);
    float st = sin(theta);
    float nxoff = map(ct, -1, 1, 0, sharpNoise);
    float nyoff = map(st, -1, 1, 0, sharpNoise);
    
    // same remark as for the noise for the radius
    //finalHue = (float) osNoise.eval((double)nxoff, (double) nyoff, (double) nzoff, (double) noise_offset_clr*10);
    finalHue = (float) osNoise.eval((double)(noise_offset_clr + nxoff), (double)(noise_offset_clr + nyoff), (double)(noise_offset_clr + nzoff));
    
    finalHue = map(finalHue, -1, 1, 0, 200)%100;
  }

  return finalHue;
}

// compute r

public float r(float angl, float n1, float n2, float n3, float a, float b, float m1, float m2) {
  float t1 = cos(m1*angl/4)/a;
  float t2 = sin(m2*angl/4)/b;
  float sum = pow(abs(t1), n2) + pow(abs(t2), n3);
  return pow(sum, -1/n1);
}

   // prevent the shape from moving when tweaking values
public void controlEvent(CallbackEvent event) {
  if (event.getAction() == ControlP5.ACTION_PRESS) {
    cam.setActive(false);
  }
  if (event.getAction() == ControlP5.ACTION_RELEASE || event.getAction() == ControlP5.ACTION_RELEASE_OUTSIDE) {
    cam.setActive(true);
  }
}
