
class Quaternion implements Cloneable {

  public float W, X, Y, Z;

  Quaternion(float w, float x, float y, float z) {
    this.W = w;
    this.X = x;
    this.Y = y;
    this.Z = z;
  }

  Quaternion(float rho, float theta, PVector pv) {   // automaticely normalize the vector
    this.W = cos(theta);
    pv.normalize();
    this.X = pv.x*sin(theta);
    this.Y = pv.y*sin(theta);
    this.Z = pv.z*sin(theta);
    this.qMult(rho);
  }
  
  Quaternion(float flt, PVector vect) {
    this.W = flt;
    this.X = vect.x;
    this.Y = vect.y;
    this.Z = vect.z;
  }
  
  Quaternion(PVector vect) {
    this(0, vect);
  }

  @Override
    public Quaternion clone() {
    return new Quaternion(W, X, Y, Z);
  }

  public Quaternion qMult(Quaternion q) {   // associated static method exists as a separated methos out of this class

    float w = this.W*q.W - this.X*q.X - this.Y*q.Y - this.Z*q.Z;
    float x = this.W*q.X + this.X*q.W + this.Y*q.Z - this.Z*q.Y;
    float y = this.W*q.Y - this.X*q.Z + this.Y*q.W + this.Z*q.X;
    float z = this.W*q.Z + this.X*q.Y - this.Y*q.X + this.Z*q.W;
    
    this.W = w;
    this.X = x;
    this.Y = y;
    this.Z = z;
    
    return this;
  }

  public Quaternion qMult(float flt) {   // associated static method exists as a separated methos out of this class

    this.W *= flt;
    this.X *= flt;
    this.Y *= flt;
    this.Z *= flt;

    return this;
  }

  public Quaternion qInv() {   // associated static method exists as a separated methos out of this class

    float normSq = this.qNormSq();

    if (normSq == 0) {
      normSq = 1;
      println("inverse operation impossible, nothing done");
      return this;
    }

    return this.qConj().qDiv(normSq);
  }

  public Quaternion qConj() {   // associated static method exists as a separated methos out of this class
    this.X = -X;
    this.Y = -Y;
    this.Z = -Z;
    return this;
  }

  public Quaternion qDiv(float flt) {   // associated static method exists as a separated methos out of this class
    if (flt == 0) {
      println("division by zero, nothing were done");
      return this;
    }

    return this.qMult(1/flt);
  }
  
  public Quaternion qNormalize() {
    return this.qDiv(this.qNorm());
  }

  public float qNorm() {
    return sqrt(this.qNormSq());
  }

  public float qNormSq() {
    float normSq = W*W + X*X + Y*Y + Z*Z;
    return normSq;
  }
}


// "static" methods

public static Quaternion qMult(Quaternion q1, Quaternion q2) {
  Quaternion q = (Quaternion) q1.clone();
  return q.qMult(q2);
}

public static Quaternion qDiv(Quaternion q1, float flt) {
  Quaternion q = (Quaternion) q1.clone();
  return q.qDiv(flt);
}

public static Quaternion qConj(Quaternion q1) {
  Quaternion q = (Quaternion) q1.clone();
  return q.qConj();
}

public static Quaternion qInv(Quaternion q1) {
  Quaternion q = (Quaternion) q1.clone();
  return q.qInv();
}

public static Quaternion qNormalize(Quaternion q1) {
  Quaternion q = q1.clone();
  return q.qNormalize();
}

public PVector qRotate(PVector pnt, PVector axis, float angle) {
  Quaternion pntQ = new Quaternion(pnt);
  Quaternion transfQ = new Quaternion(1, angle/2, axis);
  Quaternion q = qMult(transfQ, pntQ);
  q.qMult(transfQ.qInv());
  return new PVector(q.X, q.Y, q.Z);
}
