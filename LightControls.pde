
final char UP_ = 'z';
final char DOWN_ = 's';
final char LEFT_ = 'd';
final char RIGHT_ = 'q';
final char CLOCK_WISE = 'e';
final char C_CLOCK_WISE = 'f';

boolean U = false;
boolean D = false;
boolean L = false;
boolean R = false;
boolean CW = false;
boolean CCW = false;

public void updateAngles() {
  if (U) {
    xAgl = spd;
  } else if (D) {
    xAgl = -spd;
  } else { 
    xAgl = 0;
  }
  if (L) {
    yAgl = spd;
  } else if (R) {
    yAgl = -spd;
  } else { 
    yAgl = 0;
  }
  if (CCW) {
    zAgl = spd;
  } else if (CW) {
    zAgl = -spd;
  } else { 
    zAgl = 0;
  }
}

public void keyPressed() {

  if (key == ' ') {
    //float[] rotations = cam.getRotations();
    //println(degrees(rotations[0]), degrees(rotations[1]), degrees(rotations[2]));
  }

  if (key == UP_) {
    U = true;
  }
  if (key == DOWN_) {
    D = true;
  }
  if (key == LEFT_) {
    L = true;
  }
  if (key == RIGHT_) {
    R = true;
  }
  if (key == C_CLOCK_WISE) {
    CCW = true;
  }
  if (key == CLOCK_WISE) {
    CW = true;
  }
}

public void keyReleased() {

  if (key == UP_) {
    U = false;
  }
  if (key == DOWN_) {
    D = false;
  }
  if (key == LEFT_) {
    L = false;
  }
  if (key == RIGHT_) {
    R = false;
  }
  if (key == C_CLOCK_WISE) {
    CCW = false;
  }
  if (key == CLOCK_WISE) {
    CW = false;
  }
}
