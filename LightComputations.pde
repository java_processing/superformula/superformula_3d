
final PVector X_AXIS = new PVector(1, 0, 0);
final PVector Y_AXIS = new PVector(0, 1, 0);
final PVector Z_AXIS = new PVector(0, 0, 1);

PVector rotatedSource = new PVector(0, 0, -1);   // viewer position in the screen frame (absolute frame)
float xAgl = 0;
float yAgl = 0;
float zAgl = 0;
final float spd = 2;

final float distViewer = 5*r_unite;

public void computeShowLight() {

  float distLight = 50*radius;
  
  updateAngles();

  rotatedSource = qRotate(rotatedSource, X_AXIS, radians(xAgl));
  rotatedSource = qRotate(rotatedSource, Y_AXIS, radians(yAgl));
  rotatedSource = qRotate(rotatedSource, Z_AXIS, radians(zAgl));

  translate(width/2, height/2, distViewer);
  ambientLight(135, 135, 135);
  spotLight(160, 160, 160, -distLight*rotatedSource.x, -distLight*rotatedSource.y, -distLight*rotatedSource.z, distLight*rotatedSource.x, distLight*rotatedSource.y, distLight*rotatedSource.z, radians(120), 100);
}
